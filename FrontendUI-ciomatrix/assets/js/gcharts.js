	$(window).resize(function(){
  	drawChart();
	});
	
google.load("visualization", 
			"1", 
			{ packages:["corechart"]
			}
			);
      google.setOnLoadCallback(drawChart);
      function drawChart() {
	
	  /*Staple chart*/
		  
	  var data = google.visualization.arrayToDataTable([
        ["Element", "respondents", { role: "style" } ],
        ["Poor - 0", 10, "#b87333"],
        ["1", 15, "#54728c"],
        ["2", 5, "#e25856"],
        ["3", 7, "#94b86e"],
		["4", 12, "#b784c2"],
		["5", 5, "#555555"],
		["6", 8, "#ffb848"],
		["7", 10, "#435b70"],
		["8", 6, "#54818c"],
		["9", 9, "#e2dd56"],
		["Rich - 10", 5, "#6d56e2"],
      ]);
	  
	  var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

	    var options = {
        height: 450,
        legend:{
        	position:"none"
        },
        bar: {groupWidth: "90%"},
        vAxis: {
		title:'Total Number of Respondents',
    	viewWindow: {
        min: 1,
        max: 100,
   		},
    		ticks: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100] // display labels every 10
	   },
		hAxis: {
		title:'Rating Scale',
	    }
       };
      
      
      
        /* Rank Order Question*/
      
        var data1 = google.visualization.arrayToDataTable([
          ['Options', 'No. of Respondents'],
          ['Option 1', 15],
          ['Option 2', 25],
          ['Option 3', 30],
          ['Option 4', 20],
          ['Option 5', 10]
        ],false);
        var options1 = {
          pieHole: 0.3,
		  colors:['#54728c','#e25856','#94b86e','#ffb848','#852b99'],
		  'chartArea': {'width': '100%', 'height': '80%'},
		  legend : {
            position: 'none'
          }
	     };
		
		
		var data2 = google.visualization.arrayToDataTable([
          ['Options', 'No. of Respondents'],
          ['Option 1', 25],
          ['Option 2', 30],
          ['Option 3', 20],
          ['Option 4', 10],
          ['Option 5', 15]
        ],false);
        var options2 = {
          pieHole: 0.3,
		  colors:['#54728c','#e25856','#94b86e','#ffb848','#852b99'],
		  'chartArea': {'width': '100%', 'height': '80%'},
		  legend : {
            position: 'none'
          }
	     };
		
		
		var data3 = google.visualization.arrayToDataTable([
          ['Options', 'No. of Respondents'],
          ['Option 1', 30],
          ['Option 2', 20],
          ['Option 3', 10],
          ['Option 4', 15],
          ['Option 5', 25]
        ], false);
        var options3 = {
          pieHole: 0.3,
		  colors:['#54728c','#e25856','#94b86e','#ffb848','#852b99'],
		  'chartArea': {'width': '100%', 'height': '80%'},
		   legend : {
            position: 'none'
          }
		};
		
		
		var data4 = google.visualization.arrayToDataTable([
          ['Options', 'No. of Respondents'],
          ['Option 1', 20],
          ['Option 2', 10],
          ['Option 3', 15],
          ['Option 4', 25],
          ['Option 5', 30]
        ], false);
        var options4 = {
          pieHole: 0.3,
		  colors:['#54728c','#e25856','#94b86e','#ffb848','#852b99'],
		  'chartArea': {'width': '100%', 'height': '80%'},
		  legend : {
            position: 'none'
          }
	    };
		
		
		var data5 = google.visualization.arrayToDataTable([
          ['Options', 'No. of Respondents'],
          ['Option 1', 10],
          ['Option 2', 15],
          ['Option 3', 25],
          ['Option 4', 30],
          ['Option 5', 20]
        ]);
         var options5 = {
          pieHole: 0.3,
		  colors:['#54728c','#e25856','#94b86e','#ffb848','#852b99'],
		  'chartArea': {'width': '100%', 'height': '80%'},
		  legend : {
            position: 'none'
          }
        };
        
        /* Constant Sum data analysis*/
         var data6 = google.visualization.arrayToDataTable([
         ['Question', 'alloted points', { role:'style'}],
         ['A', 40, '#f4728c'],            
         ['B', 80, '#abbb70'],           
         ['C', 20, '#c4818c'],
         ['D', 50, '#435b70'],
         ['E', 30, '#b784c2'], 
      ]);
      
      	  var view6 = new google.visualization.DataView(data6);
	    
	   	  var options6 = {
          height: 250,
          legend :{
          	position:"none"
          },
          bar: {groupWidth: "50%"},
          hAxis:{
          	title:"Average of allotted points",
          },
          vAxis:{
          	title:"Options",
          },
        };
      
        
        /* Multiple choice analysis*/
         var data7 = google.visualization.arrayToDataTable([
         ['Question', 'votes', { role: 'style'}],
         ['Option A', 85, '#54728c'],            
         ['Option B', 15, '#e25856'],           
         ['Option C', 20, '#94b86e'],
         ['Option D', 50, '#ffb848'],
         ['Option E', 45, '#852b99'],
         ['Others', 30, '#fd9458'], 
      ]);
      
      	  var view7 = new google.visualization.DataView(data7);
	    	view7.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
	    
	    
	   	  var options7 = {
          height: 250,
          legend :{
          	position:"none"
          },
          bar: {groupWidth: "70%"},
          hAxis:{
            viewWindow: {
          	min: 0,
          	max: 6,
   		  },
   		  	
          },
          vAxis:{
          	title:"Number of Votes",
          	viewWindow: {
          	min: 0,
          	max: 100,
   		  	},
   		  	ticks: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100] // display labels every 10
   		   },
        };
        
        
        
        /*Contengency Analysis*/ 
        
        /*Pie-Chart Analysis*/ 
         var data8 = google.visualization.arrayToDataTable([
         ['Question', 'Answers'],
         ['Yes', 65,],            
         ['No', 35,], 
        ]);
      	 var options8 = {
      	  height: 250,
          is3D:true,
          tooltip: {
            text: 'value'
       	 },
          colors:['#852b99','#ffb848'],
        };
        
        
        
       /*Column Chart for Yes*/
        var data9 = google.visualization.arrayToDataTable([
        ["Options", "votes", { role: "style" }],
        ["A", 75, "#952b89"],
        ["B", 45, "#995b99"],
        ["C", 27, "#947867"],
		["D", 95, "#852b99"],
		["E", 15, "#999548"],
	   ]);
	  	var view9 = new google.visualization.DataView(data9);
	  	view9.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

        var options9 = {
        title:"If Yes",
        height: 300,
        legend:{
        	position:"none"	
        },
        vAxis:{
        	title:"Number of votes",
        viewWindow: {
        min: 0,
        max: 100,
   		},
   		 ticks: [0, 20, 40, 60, 80, 100]
      	},
      	hAxis: {
      		  title:'Contengent Options',
	    }
       };
       
       /*Column Chart for No*/
       var data10 = google.visualization.arrayToDataTable([
        ["Options", "votes", { role: "style" }],
        ["A", 55, "#ffc959"],
        ["B", 75, "#ffb848"],
        ["C", 27, "#ffd8ae"],
		["D", 35, "#ffd4c2"],
		["E", 45, "#ffd598"],
	   ]);
	  	var view10 = new google.visualization.DataView(data10);
	  	view10.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

        var options10 = {
        title:"If No",
        height: 300,
        legend:{
        	position:"none"	
        },
        vAxis:{
        	title:"Number of votes",
        viewWindow: {
        min: 0,
        max: 100,
   		},
   		 ticks: [0, 20, 40, 60, 80, 100]
      	},
      	hAxis: {
		  title:'Contengent Options',
	    }
       };
        
        
		
		/*VARIABLE DECLARATIONS FOR GOOGLE CHARTS*/ 
		
		/*Staple chart*/
		var chart = new google.visualization.ColumnChart(document.getElementById("staple"));
     	chart.draw(view, options);	
		
		/*Rank Order Question*/
		var chart = new google.visualization.PieChart(document.getElementById("rank1"));
        chart.draw(data1, options1);
        
        var chart = new google.visualization.PieChart(document.getElementById("rank2"));
        chart.draw(data2, options2);
        
        var chart = new google.visualization.PieChart(document.getElementById("rank3"));
        chart.draw(data3, options3);
		
		var chart = new google.visualization.PieChart(document.getElementById("rank4"));
        chart.draw(data4, options4);
        
		var chart = new google.visualization.PieChart(document.getElementById("rank5"));
        chart.draw(data5, options5);
				
		/*Constant Sum question*/
		var chart = new google.visualization.BarChart(document.getElementById("constant_sum"));
        chart.draw(view6, options6);
		
		/*Multiple choice analysis*/
		var chart = new google.visualization.ColumnChart(document.getElementById("multiple_choice"));
        chart.draw(view7, options7);
        
		/*Contengency Question*/
		var chart = new google.visualization.PieChart(document.getElementById("contengency_1"));
        chart.draw(data8, options8);
		
		var chart = new google.visualization.ColumnChart(document.getElementById("contengency_2"));
        chart.draw(view9,options9);
        
        var chart = new google.visualization.ColumnChart(document.getElementById("contengency_3"));
        chart.draw(view10,options10);
	}
