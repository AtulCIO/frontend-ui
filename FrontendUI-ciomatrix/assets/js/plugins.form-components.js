/*
 * Core script to handle all form specific plugins
 */

var FormComponents = function() {

	"use strict";

	
	/**************************
	 * Uniform                *
	 **************************/
	var initUniform = function() {
		if ($.fn.uniform) {
			$(':radio.uniform, :checkbox.uniform').uniform();
		}
	}

	
	return {

		// main function to initiate all plugins
		init: function () {
			initUniform(); // Uniform (styled radio- and checkboxes)
		}

	};

}();