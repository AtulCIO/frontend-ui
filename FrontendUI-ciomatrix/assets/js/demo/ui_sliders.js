
$(document).ready(function(){

	// Range with fixed minimum
	$( '#slider-range-min' ).slider({
		range: 'min',
		value: 0,
		min: 0,
		max: 30,
		slide: function( event, ui ) {
			$( '#slider-range-min-amount' ).text( ui.value );
		}
	});
	$('#slider-range-min-amount').text( $('#slider-range-min').slider('value'));

});