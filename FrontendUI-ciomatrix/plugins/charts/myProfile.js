	var circle = {
		width : 245,height : 225,diameter : 150,duration : 1000,
		zeroAngle : 0,fullAngle : 360 * (Math.PI/180),
		outerArc : d3.svg.arc(),innerArcRest : d3.svg.arc(),
		innerArc : d3.svg.arc(),outerArc1 : d3.svg.arc(),margin : 0,
		generateCords : function() {
			circle.outerArc.outerRadius(circle.diameter/2);
			circle.outerArc.innerRadius(circle.diameter/2 * 0.85);
			circle.outerArc.startAngle(circle.zeroAngle);
			circle.outerArc.endAngle(circle.zeroAngle);
			circle.outerArc1.outerRadius(circle.diameter/2);
			circle.outerArc1.innerRadius(circle.diameter/2 * 0.85);
			circle.outerArc1.endAngle(circle.fullAngle);
			circle.innerArc.outerRadius(circle.diameter/2 * 0.85);
			circle.innerArc.innerRadius(circle.diameter/2 * .85 - (circle.diameter/2 * .15));
			circle.innerArc.startAngle(circle.zeroAngle);
			circle.innerArc.endAngle(circle.zeroAngle);
			circle.innerArcRest.outerRadius(circle.diameter/2 * 0.85);
			circle.innerArcRest.innerRadius(circle.diameter/2 * .85 - (circle.diameter/2 * .15));
			circle.innerArcRest.startAngle(circle.zeroAngle);
			circle.innerArcRest.endAngle(circle.zeroAngle);
			circle.margin = (circle.diameter + String(circle.data.totalEmp).length * 20)/2;
			
		},
		generateCircle : function() {
			var svg = d3.select("."+circle.data.eleClass).append("svg")
						.attr("width", circle.width)
						.attr("height", circle.height);
			var ticks = svg.append("g").selectAll("g")
							.data([{startAngle:0,endAngle:circle.fullAngle,totalEmp:circle.data.totalEmp}])
							.enter().append("g").attr("transform", "translate(" + circle.margin + "," + circle.margin + ")").selectAll("g")
							.data(circle.groupTicks)
							.enter().append("g")
							.attr("transform", function(d) {
								return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
										+ "translate(" + circle.diameter/2 + ",0)";
							});
			ticks.append("line")
				.attr("x1", 1)
				.attr("y1", 0)
				.attr("x2", 5)
				.attr("y2", 0)
				.attr("class", "dailTics");

			ticks.append("text")
				.attr("x", 8)
				.attr("dy", ".35em")
				.attr("class", "dailText")
				.attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180)translate(-16)" : null; })
				.style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
				.text(function(d) { return d.label; });
			var staticArcs = svg.append("g")
								.attr("class", "staticArcs")
								.attr("transform", "translate(" + circle.margin + "," + circle.margin + ")");
			circle.outerArc1.startAngle(circle.equalAngles[1].angle);
			staticArcs.append("path")
					.attr("class", "staticOuterArc")
					.attr("d", circle.outerArc1);
			var arcs = svg.append("g").attr("class", "arcs").attr("transform", "translate(" + circle.margin + "," + circle.margin + ")"),
				innerArcPath = arcs.append("path"),
				outerArcPath = arcs.append("path"),
				innerArcRestPath = arcs.append("path"),
				singleEmpAngle = circle.getSingleEmpAngle({startAngle:circle.equalAngles[1].angle,endAngle:circle.fullAngle,totalEmp:circle.data.totalEmp}),
				dailTooltip = d3.select("#dailTooltip");
			outerArcPath.attr("class", "outerArc")
						.attr("d", circle.outerArc)
						.datum(circle.fullAngle);
			innerArcPath.attr("class", "innerArc")
						.attr("d", circle.innerArc)
							.on("mouseover", function(){ dailTooltip.text(circle.data.profileMatch); return dailTooltip.style("visibility", "visible");})
							.on("mousemove", function(d){return dailTooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");})
							.on("mouseout", function(){return dailTooltip.style("visibility", "hidden");})
						.datum(singleEmpAngle * circle.data.profileMatch + circle.equalAngles[1].angle);
			innerArcRestPath.attr("class", "innerArcRest")
						.attr("d", circle.innerArcRest)
						.datum(circle.fullAngle);
			circle.outerArc.startAngle(circle.equalAngles[1].angle);
			circle.innerArc.startAngle(circle.equalAngles[1].angle);
			circle.innerArcRest.startAngle(singleEmpAngle * circle.data.profileMatch + circle.equalAngles[1].angle);
			outerArcPath.transition().duration(circle.duration)
						.attrTween("d", circle.outerArcTween);
			innerArcPath.transition().delay(circle.duration).duration(circle.duration)
						.attrTween("d", circle.innerArcTween);
			innerArcRestPath.transition().duration(circle.duration)
						.attrTween("d", circle.innerArcRestTween);
		},
		groupTicks : function(data) {
			var equalAngles =  circle.getAngles(data, 11),
				labels = d3.range(0, data.totalEmp, data.totalEmp/10);
			labels.unshift(data.totalEmp);	
			for(var i=0 ; i<equalAngles.length; i++) {
				equalAngles[i].label = Math.round(labels[i]);
				circle.fullAngle == equalAngles[i].angle ? equalAngles.pop(i) : '';
			}
			circle.equalAngles = equalAngles;
			return equalAngles;
		},
		getAngles : function(data, distribution) {
			var oneEmp = circle.getSingleEmpAngle(data);
			return d3.range(0, data.totalEmp, data.totalEmp/distribution).map(function(empRange, i) {
				return {
					angle: empRange * oneEmp
				};
			});
		},
		getSingleEmpAngle : function(d) {
			return (d.endAngle - d.startAngle) / d.totalEmp;
		},
		innerArcTween : function(a) {
			return circle.arcTween(circle.innerArc,a);
		},
		innerArcRestTween : function(a) {
			return circle.arcTween(circle.innerArcRest,a);
		},
		outerArcTween : function(a) {
			return circle.arcTween(circle.outerArc,a);
		},
		arcTween : function(currentArc,a) {
			var i = d3.interpolate(0, a);
			return function(t) {
				return currentArc.endAngle(i(t))();
			};
		}

	}
	var bulletCharts = {
		svgWidth : 960, svgHeight : 150,duration : 1000,chartWidth : 720,
		margin : {top: 50, right: 100, bottom: 20, left: 75},
		data : [{title : "Interest 1",total : 500,completed : 100,inprogress : 300},{title : "Interest 2",total : 700,completed : 450,inprogress : 200}],
		reverse : false,
		generateCords : function() {
			bulletCharts.bulletWidth = bulletCharts.chartWidth - bulletCharts.margin.left - bulletCharts.margin.right;
			bulletCharts.bulletHeight = 25; 
			bulletCharts.widthScale = bulletCharts.getWidthScale([0, 700]);
			bulletCharts.svgHeight = Math.max(bulletCharts.svgHeight,bulletCharts.data.length * 100);
		},
		renderSvgs : function(ele) {
			var svg,bulletGroup,title,xaxis,yaxis,axisSvg,xaxisGroup,yaxisGroup;
			svg = d3.select("."+ele).append("svg")
						.attr("class", "bulletChart")
						.attr("width", bulletCharts.svgWidth)
						.attr("height", bulletCharts.svgHeight);
			
			bulletGroup = d3.select(".bulletChart").selectAll("g")
				.data(bulletCharts.data)
				.enter().append("g")
				.attr("transform", function(d,i) { return "translate(" + bulletCharts.margin.left + "," + (bulletCharts.margin.top + i*75) + ")"} )
				.call(bulletCharts.renderChart);

			title = bulletGroup.append("g")
				.style("text-anchor", "end")
				.attr("transform", "translate(-6," + bulletCharts.bulletHeight / 2 + ")");
			
			title.append("text")
				.attr("class", "title")
				.text(function(d) { return d.title; });
	
			var legend = svg.append("g").attr("class","legends")
							.attr("transform", "translate(" + (bulletCharts.chartWidth) + "," + bulletCharts.margin.top + ")"),
				legendData = [{type : "total", label: "Total EBs Matching Profile", y:0},{type : "inprogress", label: "EBs In-Progress", y:42},{type:"completed",label:"EBs Completed", y:74}];
				
				for(var i=0; i<legendData.length; i++) {
				legend.append("rect")
					.attr("width", bulletCharts.bulletHeight + 5)
					.attr("class", legendData[i].type)
					.attr("height", i == 0 ? bulletCharts.bulletHeight : bulletCharts.bulletHeight/3)
					.attr("y", legendData[i].y);
				legend.append("text")
					.attr("x", 40)
					.attr("y", (2*i + 1) * 16.5)
					.text(legendData[i].label);
				}
		},
		renderChart : function(g) {
			g.each(function(data, index) {
				var total = bulletCharts.getTotal.call(this, data, index).slice().sort(d3.descending),
					currentStatus = bulletCharts.getCurrentStatus.call(this, data, index).slice().sort(d3.descending),
					g = d3.select(this);
				
				/* Compute the new x-scale. */
				bulletCharts.widthScale = bulletCharts.getWidthScale([0,Math.max(total[0],currentStatus[0])]);
				
				/* To retrieve the old x-scale, if this is an update.*/
				var x0 = this.__chart__ || d3.scale.linear().domain([0, Infinity]).range(bulletCharts.widthScale.range());
				
				// Stash the new scale
				this.__chart__ = bulletCharts.widthScale;
				
				// Deriving width-scales from the x-scales.
				var w0 = bulletCharts.getBulletWidth(x0),w1 = bulletCharts.getBulletWidth(bulletCharts.widthScale);
				
				var currenStatusLabel = bulletCharts.getCurrentStatusLabel(data,currentStatus);
				
				bulletCharts.renderRect({g:g, data:total, className:'total', classLabel:[''], bulletHeight:bulletCharts.bulletHeight, x0:x0, w0:w0, w1:w1});
				bulletCharts.renderRect({g:g, data:currentStatus, className:'currentStatus', classLabel:currenStatusLabel, bulletHeight:bulletCharts.bulletHeight/3, x0:0, w0:w0, w1:w1});
				
				 // Compute the tick format.
				  var format = bulletCharts.widthScale.tickFormat(8);

				  // Update the tick groups.
				  var tick = g.selectAll("g.tick")
					  .data(bulletCharts.widthScale.ticks(8), function(d) {
						return this.textContent || format(d);
					  });

				  // Initialize the ticks with the old scale, x0.
				  var tickEnter = tick.enter().append("g")
					  .attr("class", "tick")
					  .attr("transform", bulletCharts.getBulletTranslate(x0))
					  .style("opacity", 1e-6);

				  tickEnter.append("line")
					  .attr("y1", bulletCharts.bulletHeight)
					  .attr("y2", bulletCharts.bulletHeight * 7 / 6);

				  tickEnter.append("text")
					  .attr("text-anchor", "middle")
					  .attr("dy", "1em")
					  .attr("y", bulletCharts.bulletHeight * 7 / 6)
					  .text(format);

				  // Transition the entering ticks to the new scale, bulletCharts.widthScale.
				  tickEnter.transition()
					  .duration(bulletCharts.duration)
					  .attr("transform", bulletCharts.getBulletTranslate(bulletCharts.widthScale))
					  .style("opacity", 1);

				  // Transition the updating ticks to the new scale, bulletCharts.widthScale.
				  var tickUpdate = tick.transition()
					  .duration(bulletCharts.duration)
					  .attr("transform", bulletCharts.getBulletTranslate(bulletCharts.widthScale))
					  .style("opacity", 1);

				  tickUpdate.select("line")
					  .attr("y1", bulletCharts.bulletHeight)
					  .attr("y2", bulletCharts.bulletHeight * 7 / 6);

				  tickUpdate.select("text")
					  .attr("y", bulletCharts.bulletHeight * 7 / 6);

				  // Transition the exiting ticks to the new scale, bulletCharts.widthScale.
				  tick.exit().transition()
					  .duration(bulletCharts.duration)
					  .attr("transform", bulletCharts.getBulletTranslate(bulletCharts.widthScale))
					  .style("opacity", 1e-6)
					  .remove();
				
			});
		},
		renderRect : function(paramObj) {
			var totalRect = paramObj.g.selectAll("rect."+paramObj.className).data(paramObj.data);
			
			totalRect.enter().append("rect")
				.attr("class", function(d, i) { return paramObj.classLabel[i] +" "+ paramObj.className; })
				.attr("width", paramObj.w0)
				.attr("height", paramObj.bulletHeight)
				.attr("x", bulletCharts.reverse ? paramObj.x0 : 0)
				.attr("y", paramObj.className == 'total' ? 0 : paramObj.bulletHeight)
				.transition()
				.duration(bulletCharts.duration)
				.attr("width", paramObj.w1)
				.attr("x", bulletCharts.reverse ? bulletCharts.widthScale : 0);
			
			totalRect.transition()
			  .duration(bulletCharts.duration)
			  .attr("x", bulletCharts.reverse ? bulletCharts.widthScale : 0)
			  .attr("width", paramObj.w1)
			  .attr("height", paramObj.bulletHeight)
			  .attr("y",paramObj.className == 'total' ? 0 : paramObj.bulletHeight);
		},
		getWidthScale : function(domain) {
			return d3.scale.linear()
					.domain(domain)
					.range(bulletCharts.reverse ? [bulletCharts.bulletWidth, 0] : [0, bulletCharts.bulletWidth]);
		},
		getHeightScale : function(domain) {
			return d3.scale.linear()
					.domain(domain)
					.range(bulletCharts.reverse ? [0, domain[1]] : [domain[1], 0]);
		},
		getTotal : function(d) {
			return [d.total];
		},
		getCurrentStatus : function(d) {
			return [d.completed,d.completed+d.inprogress];
		},
		getBulletWidth : function(x) {
		  var x0 = x(0);
		  return function(d) {
			return Math.abs(x(d) - x0);
		  };
		},
		getBulletTranslate : function(x) {
		  return function(d) {
			return "translate(" + x(d) + ",0)";
		  };
		},
		getCurrentStatusLabel : function(data,currentStatus) {
			var label = [];
			if(currentStatus[0] == data.completed) {
				label = ["completed","inprogress"];
			} else {
				label = ["inprogress","completed"];
			}
			return label;
		},
		init : function(ele) {
			bulletCharts.generateCords();
			bulletCharts.renderSvgs(ele);
		}
	}
	var totalEmp = getEle("totalEmps"),profileMatch = getEle("profileMatches"),
		allInputs = document.getElementsByTagName("input");
	for(var i=0; i<allInputs.length; i++) {
		allInputs[i].onkeyup = function(e) {
			if(e.keyCode == 13 && e.target.name == "updateProfile") {
				updateProfile();
			} else if(e.keyCode == 13 && e.target.name == "updateBulletChart") {
				updateBulletChart();
			}
		}
	}
	function getEle(id) {
		return document.getElementById(id);
	}
	function updateBulletChart(e) {
		var data = {},executiveBriefings = [];
		data.totalEmp = parseInt(totalEmp.value) > 0 ? parseInt(totalEmp.value) : 1000;
		for(var i=1 ; i<=3; i++) {
			var aoi = getEle("aoi_"+i),totalEBs = getEle("totalEBs_"+i),ebsCompleted = getEle("ebsCompleted_"+i),ebsInprogress = getEle("ebsInprogress_"+i);
			if(aoi.value != "" && parseInt(totalEBs.value) > 0 && parseInt(ebsCompleted.value) > 0 && parseInt(ebsInprogress.value) > 0) {
				executiveBriefings.push({title : aoi.value,total : parseInt(totalEBs.value),completed : parseInt(ebsCompleted.value),inprogress : parseInt(ebsInprogress.value)});
			}
		}
		data.executiveBriefings = executiveBriefings;
		bulletCharts.data = executiveBriefings;
		document.getElementsByClassName("executiveBriefings")[0].innerHTML = ""
		bulletCharts.init("executiveBriefings");
	}
	function updateProfile(e) {
		if(parseInt(totalEmp.value) > parseInt(profileMatch.value)) {
			circle.data = {eleClass: "circleWithTics", totalEmp : totalEmp.value, profileMatch : profileMatch.value};
			document.getElementsByClassName("circleWithTics")[0].innerHTML = ""
			circle.generateCords();
			circle.generateCircle();
		} else {
			alert("Please input correct data");
		}
	}
	circle.data = {eleClass: "circleWithTics", totalEmp : 1000, profileMatch : 250};
	circle.generateCords();
	circle.generateCircle();
	bulletCharts.init("executiveBriefings");