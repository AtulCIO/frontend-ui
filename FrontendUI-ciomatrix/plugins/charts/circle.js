var circle = {};

circle = {
	width : 300,height : 225,diameter : 150,duration : 1000,
	zeroAngle : 0,fullAngle : 360 * (Math.PI/180),
	outerArc : d3.svg.arc(),
	innerArc : d3.svg.arc(),margin : 0,
	generateCords : function() {
		circle.outerArc.outerRadius(circle.diameter/2);
		circle.outerArc.innerRadius(circle.diameter/2 * 0.85);
		circle.outerArc.startAngle(circle.zeroAngle);
		circle.outerArc.endAngle(circle.fullAngle);
		circle.innerArc.outerRadius(circle.diameter/2 * 0.85);
		circle.innerArc.innerRadius(circle.diameter/2 * .85 - (circle.diameter/2 * .15));
		circle.innerArc.startAngle(circle.zeroAngle);
		circle.innerArc.endAngle(circle.zeroAngle);
		circle.margin = (circle.diameter + String(circle.data.totalEmp).length * 20)/2;
	},
	
	generateCircle : function() {
		var svg = d3.select("."+circle.data.eleClass).append("svg")
					.attr("width", circle.width)
					.attr("height", circle.height);
		var ticks = svg.append("g").selectAll("g")
						.data([{startAngle:0,endAngle:circle.fullAngle,totalEmp:circle.data.totalEmp}])
						.enter().append("g").attr("transform", "translate(" + circle.margin + "," + circle.margin + ")").selectAll("g")
						.data(circle.groupTicks)
						.enter().append("g")
						.attr("transform", function(d) {
							return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
									+ "translate(" + circle.diameter/2 + ",0)";
						});
		ticks.append("line")
			.attr("x1", 1)
			.attr("y1", 0)
			.attr("x2", 5)
			.attr("y2", 0)
			.attr("class", "dailTics");

		ticks.append("text")
			.attr("x", 8)
			.attr("dy", ".35em")
			.attr("class", "dailText")
			.attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180)translate(-16)" : null; })
			.style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
			.text(function(d) { return d.label; });
		
		svg.append("g")
			.attr("class", "component")
			.attr("transform", "translate(" + circle.margin + "," + circle.margin + ")")
			.append("path")
			.attr("d", circle.outerArc);
		
		circle.outerArc.endAngle(circle.zeroAngle);
		var arcs = svg.append("g").attr("class", "arcs").attr("transform", "translate(" + circle.margin + "," + circle.margin + ")"),
			innerArcPath = arcs.append("path"),
			outerArcPath = arcs.append("path");
		
		outerArcPath.attr("class", "outerArc")
					.attr("d", circle.outerArc);
		innerArcPath.attr("class", "innerArc")
					.attr("d", circle.innerArc);
		
		outerArcPath.datum(circle.fullAngle);
		outerArcPath.transition().duration(circle.duration)
                    .attrTween("d", circle.outerArcTween);
		
		
			innerArcPath.datum(circle.getSingleEmpAngle({startAngle:0,endAngle:circle.fullAngle,totalEmp:circle.data.totalEmp}) * circle.data.profileMatch);
			innerArcPath.transition().delay(circle.duration).duration(circle.duration)
						.attrTween("d", circle.innerArcTween);
		
					
		
	},
	groupTicks : function(d) {
	  var oneEmp = circle.getSingleEmpAngle(d);
	  return d3.range(0, d.totalEmp, d.totalEmp/10).map(function(empRange, i) {
		return {
		  angle: empRange * oneEmp,
		  label: empRange == 0 ? d.totalEmp : empRange
		};
	  });
	},
	getSingleEmpAngle : function(d) {
		return (d.endAngle - d.startAngle) / d.totalEmp;
	},
	innerArcTween : function(a) {
		return circle.arcTween(circle.innerArc,a)
	},
	outerArcTween : function(a) {
		return circle.arcTween(circle.outerArc,a)
	},
	arcTween : function(currentArc,a) {
        var i = d3.interpolate(0, a);
		return function(t) {
            return currentArc.endAngle(i(t))();
        };
    }

}
var totalEmp = document.getElementById("totalEmps"),
	profileMatch = document.getElementById("profileMatches");
	
totalEmp.onkeyup = updateProfile;
profileMatch.onkeyup = updateProfile;

function updateProfile(e) {
	if(e.keyCode == 13) {
		if(parseInt(totalEmp.value) > parseInt(profileMatch.value)) {
			circle.data = {eleClass: "circleWithTics", totalEmp : totalEmp.value, profileMatch : profileMatch.value};
			document.getElementsByClassName("circleWithTics")[0].innerHTML = ""
			circle.generateCords();
			circle.generateCircle();
		} else {
			alert("Please input correct data");
		}
	}
}
circle.data = {eleClass: "circleWithTics", totalEmp : 1000, profileMatch : 500};
circle.generateCords();
circle.generateCircle();