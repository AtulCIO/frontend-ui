
function funcToggleCookie(u,chkbox){
    if ($(chkbox).is(':checked')  == true) {
        funcSetCookie($(u).val(), 30);
    } else {
        funcSetCookie("", -1);
    }
}

function funcSetCookie(cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = "ciousername="+cvalue+"; "+expires;
}

function funcGetCookie() {
    var name = "ciousername=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function toggleerrorlbl(lbl,lblerror){
    if ($(lbl).hasClass( "has-error" ) || $(lbl).hasClass( "has-error1" )) {
        $(lblerror).css('display', 'inline');
    } else {
        $(lblerror).css('display', 'none');
    }
}

function toggleerrorlbl2(lbl,lblerror){
    if ($(lbl).hasClass( "has-error" ) || $(lbl).hasClass( "has-error1" )) {
        $(lblerror).html("Invalid email address.");
    } else {
        $(lblerror).html(" ");
    }
}

function funcCheckCookie() {
    var user=funcGetCookie("ciousername");
    if (user != "") {
        $('#username').val(user);
        $('#remember').prop('checked', true);
    }
    $('#remember').prop('checked', false);
}

function funcTrim(id){
    $(id).val($(id).val().trim());
}

function funcNullCheck(id,lbl){
    var str = $(id).val().length;
    if ( str <= 0 ) {
        $(lbl).addClass("has-error1");
    } else {
        $(lbl).removeClass("has-error1");
    }
}

function funcTrimNull (id,lbl) {
    funcTrim(id);
    funcNullCheck(id,lbl);
}

function funcRemoveErrorClass(){
    if (localStorage.chkbx && localStorage.chkbx != '') {
        $('#rememberme').attr('checked', 'checked');
        $('#username').val(localStorage.un);
        $('#password').val(localStorage.pw);
    } else {
        $('#rememberme').removeAttr('checked');
        $('#username').val('');
        $('#password').val('');
        $('.form-group').removeClass('has-error1');
        $('.form-horizontal').trigger("reset");
    }

    $('#forgotpass').css('display', 'none');
    $('#loginuser').css('display', 'block')

}

function funcCheckEmail(sEmail,lbl){

    var r=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var valid = r.test($(sEmail).val());

    if (valid) {
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
        return;
    }
}

function funcDoLogin() {
    var rootURL = "/rest/member/memberlogin";
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"email":"' +  $('#username').val() +
                '","password":"' + hex_sha1($('#password').val()) +
              '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            if (data.message == "Login Successful") {
                alert("Login Successful");
            } else if (data.message == "Member already logged in") {
                alert("Member already logged in");
            } else {
                alert("Incorrect Username or Password");
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('User not Registered.');
        }
    });
}

function funcDoForgotPass() {
    var rootURL = "/rest/member/forgotpasswordrequest";
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"email":"' +  $('#form2username').val() + '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            if (data.errorMessage == "Done") {
                alert("Please check your email for further instruction");
                $('#login').modal('hide');
            } else {
//                alert("User not Registered.");
                $("#lblbelow_form2username").html("User not Registered.");
                $("#lblform2username").addClass("has-error");
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
//            console.log(JSON.stringify(jqXHR));
//            alert('User not Registered.');
            $("#lblbelow_form2username").html("User not Registered.");
            $("#lblform2username").addClass("has-error");
        }
    });
}



