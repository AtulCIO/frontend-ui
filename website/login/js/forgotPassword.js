//forgot password javascript code


function validatePwdOnType(id,lbl) {

    var val = $(id).val();
    var pattern = /^[a-zA-Z0-9\$\.\!\%\^\*\@]*$/;

//    console.log(pattern.test(val));
//    var pattern = /^[\w\$\.\!\%\^\*]*$/i;
    if (pattern.test(val)) {
        $(lbl).removeClass("has-error");
    } else {
        $("#lblbelow_password").html("Invalid characters.");
        $(lbl).addClass("has-error");
    }
    var val = $(id).val();
    if (val.length == 0) {
        funcCheckBoth($("#lblconfirmpassword"));
        toggleerrorlbl($("#lblconfirmpassword"),$("#lblbelow_confirmpassword"));
    }
}

function validatePwdOnBlur(id,lbl) {

    var val = $(id).val();

    if (val.length == 0) {
        $("#lblbelow_password").html("Password too short.");
        $(lbl).addClass("has-error");
        return;
    } else {
        $(lbl).removeClass("has-error");
    }

    var pattern = /^[a-zA-Z0-9\$\.\!\%\^\*\@]*$/;
    if (pattern.test(val)) {
        $(lbl).removeClass("has-error");
        if (val.length < 8) {
            $("#lblbelow_password").html("Password too short.");
            $(lbl).addClass("has-error");
        } else {
            var pattern = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[\$\.\!\%\^\*\@]).{8,14}$/i;
            if (pattern.test(val)) {
                $(lbl).removeClass("has-error");
            } else {
                $("#lblbelow_password").html("Invalid characters.");
                $(lbl).addClass("has-error");
                return;
            }
        }

    } else {
        $("#lblbelow_password").html("Invalid characters.");
        $(lbl).addClass("has-error");
        return;
    }

}

function funcCheckBoth(lbl) {

    if ($("#password").val() == $("#confirmpassword").val()) {
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
    }
}

function toggleerrorlbl(lbl,lblerror){
    if ($(lbl).hasClass( "has-error" )) {
        $(lblerror).css('display', 'inline');
    } else {
        $(lblerror).css('display', 'none');
    }
}

function funcValidateKey(key) {

    var rootURL = "/rest/member/validateforgotkey/" + key;
    var newarr = [];
    $.ajax({
        type: 'GET',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
//        data: "",
        success: function(data, textStatus, jqXHR){
//        console.log(data);
        if (data.errorMessage == "expired") {
            $('#expired').show();
            $("#proceedforgot").attr('disabled','disabled');
        } else {

        }

        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            $('#expired').show();
            $("#proceedforgot").attr('disabled','disabled');
//            alert('Custom error: check logs');
        }
    });
}


function funcPasswordChange(key) {

    var rootURL = "/rest/member/forgotpasswordsubmit";
    var newarr = [];
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"authKey":"' +  key +
                '","password":"' + $('#password').val() +
              '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(data);

            if (data.errorMessage == "passwordchanged") {
                $('#beforeprocess').css('display', 'none');
                $('#afterprocess').css('display', 'inline');
                $('#proceedforgot').css('display', 'none');
                $('#home').css('display', 'inline');

            } else if (data.errorMessage == "expired") {
                $('#expired').show();
                $("#proceedforgot").attr('disabled','disabled');
            }

        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            $('#expired').show();
            $("#proceedforgot").attr('disabled','disabled');
//            alert('Custom error: check logs');
        }
    });
}