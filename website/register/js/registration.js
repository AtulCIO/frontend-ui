
//functions related to registration form
function funcTrim(id){
    $(id).val($(id).val().trim());
}

function funcNullCheck(id,lbl){
    var str = $(id).val().trim().length;
    if ( str <= 0 ) {
        $(lbl).addClass("has-error");
    } else {
        $(lbl).removeClass("has-error");
    }
}

function funcCompanyNullCheck(id,lbl){
//    var str = $(id).val().trim().length;
//    if ( str <= 0 ) {
//        $(lbl).addClass("has-error");
//    }


  var val = $(id).val();
    var pattern = /^[^ ][\w\-\ \,\.\?\@\&\!\#\~\*\_\'\;\+]*$/i;
    if (pattern.test(val)) {
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
    }
}

function funcSelectNullCheck(id,lbl){
    if ($(id)[0].selectedIndex == -1) {
        $(lbl).addClass("has-error");
    } else {
        $(lbl).removeClass("has-error");
    }

}

function funcNamePatternCheck(id,lbl) {

    var val = $(id).val();
    if (/^[a-z][a-z\ \.]*[a-z]*$/i.test(val)){
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
    }

}

function funcMobileNumberPatternCheck(id,lbl) {
    funcTrim(id);
    var val = $(id).val();
    if (/^(\d{1,12})?$/.test(val)){
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
    }
    if (val == "") {
        $(lbl).addClass("has-error");
    }
}
function toggleerrorlbl(lbl,lblerror){
    if ($(lbl).hasClass( "has-error" )) {
        $(lblerror).css('display', 'inline');
    } else {
        $(lblerror).css('display', 'none');
    }
}


function funcStdNumberPatternCheck(id,lbl) {
    funcTrim(id);
    var val = $(id).val();
    if (/^(\d{0,3})?$/.test(val)){
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
    }
    if (val == "") {
        $(lbl).addClass("has-error");
    }
}

function funcTrimNullPattern(id,lbl){
    funcTrim(id);
    funcNullCheck(id,lbl);
    funcNamePatternCheck(id,lbl);
}

function funcTrimNull (id,lbl) {
    funcTrim(id);
    funcNullCheck(id,lbl);
}

function funcValidateEmail(sEmail,lbl) {
    funcTrimNull(sEmail,lbl);

    var r=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var valid = r.test($(sEmail).val());

//    var x = $(sEmail).val();
//    var atpos = x.indexOf("@");
//    var dotpos = x.lastIndexOf(".");
//    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
//        valid = false;
//    }

    // should bebefore next for block
    if (valid) {
        $(lbl).removeClass("has-error");
    } else {
        $(lbl).addClass("has-error");
        return;
    }

    for(var i = 0; i < blockedDomain.length; i++) {

//        if (!$(sEmail).val().contains('@') || !$(sEmail).val().contains('.')) {
//            $(lbl).addClass("has-error");
//            return;
//        }
        var domain = /[@](.*)[.]/.exec($(sEmail).val())[1];

        if(domain.toLowerCase() == blockedDomain[i].value) {
            $(lbl).addClass("has-error");
            return;
        } else {
            $(lbl).removeClass("has-error");
        }
    }
//
//    var expr = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
//    var valid = expr.test($(sEmail).val());


}

function funcRScheckBlockedDomain(){

    var rootURL = "/rest/registration/getblockeddomains"
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
//        data: "",
        success: function(data, textStatus, jqXHR){
            for (i=0; i<data.length; ++i) {
                blockedDomain.push({name: data[i], value: data[i]});
            }
//            console.log(blockedDomain);
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });
}

function closeTCModal() {
    $('#tc').modal('hide');
}

function checkBeforeClose() {
    if ($('#afterregister').css('display') == 'none') {
        if (confirm("Are you sure you want to leave this page?")) {
            funcRemoveErrorClass();
            $('#signup').modal('hide');
        }
    } else {
        funcRemoveErrorClass();
        $('#signup').modal('hide');
    }
//        $( id ).click(function( event ) {
//            event.preventDefault();
//        });
}

function funcZipcodePatternCheck(id,lbl) {
    funcTrim(id);
    var val = $(id).val();

    if (val == "") {
        $(lbl).removeClass("has-error");
    } else {
        if (/^[a-z0-9]+[a-z0-9|\-]+[a-z0-9]$/i.test(val)){
            $(lbl).removeClass("has-error");
        } else {
            $(lbl).addClass("has-error");
        }
    }
}

function updatelblbelow(firstName){
    $('#lblbelowemail').html($(firstName).val().trim());
    $('#lblbelowmobile').html($(firstName).val().trim());
    $('#lblbelowhowdouknow').html($(firstName).val().trim());
}

function funcRemoveErrorClass(){
    $('.help-on-error').css('display', 'none');
    $('.form-group').removeClass('has-error');
    $('.form-horizontal').trigger("reset");
    $('#lblbelowemail').html("");
    $('#lblbelowmobile').html("");
    $('#lblbelowhowdouknow').html("");
    $('#country')[0].selectedIndex = -1;
    $('#state')[0].selectedIndex = -1;
    $('#city')[0].selectedIndex = -1;
    $('#howdidyouknow')[0].selectedIndex = -1;
    $('#beforeregister').show();
    $('#afterregister').hide();

    if  (userCountry != "") {
        $("#country").val(userCountry);
        funcRSgetStates($("#country").val());
    }
}


function funcRSgetCompany(){

    var rootURL = "/admin/rest/company/getcompanylist"
    var newarr = [];
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
//        data: "",
        success: function(data, textStatus, jqXHR){
            for (i=0; i<data.length; ++i) {
                newarr.push({name: data[i].companyName, value: data[i].companyName});
            }
//            console.log(newarr);
            $('#company').autocomplete({
                lookup: newarr
            });

//            working code
//            for (i=0; i<data.length; ++i) {
//                $("#company").append("<option value='" + data[i].companyCode + "'>" + data[i].companyName + "</option>")
//            }
//            working code

        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });

}

function funcRSgetDesignation(){

    var rootURL = "/rest/registration/getdesignations"
    var newarr = [];
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
//        data: "",
        success: function(data, textStatus, jqXHR){
            for (i=0; i<data.length; ++i) {
                newarr.push({name: data[i], value: data[i]});
            }
//            console.log(newarr);
            $('#designation').autocomplete({
                lookup: newarr
            });

//            working code
//            for (i=0; i<data.length; ++i) {
//                $("#company").append("<option value='" + data[i].companyCode + "'>" + data[i].companyName + "</option>")
//            }
//            working code

        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });

}

function funcRSgetCountries(){

    var rootURL = "/rest/api/location/getcountries"
    var newarr = [];
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
//        data: "",
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            for (i=0; i<data.length; ++i) {
                $("#country").append("<option value='" + data[i].countryCode + "'>" + data[i].countryName + "</option>")
                isdCode.push({id:data[i].countryCode,isd:data[i].countryISDCode});
            }
            $('#country option').sort(NASort).appendTo('#country');
            $("#country")[0].selectedIndex = -1;
            funcRSgetIp2country();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });

}


function funcRSgetIp2country(){

    var rootURL = "/rest/api/location/getip2country"
    var newarr = [];
    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            if (data.countryCode != "-") {
                userCountry = data.countryCode;
                $("#country").val(userCountry);
                funcRSgetStates($("#country").val());
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });

}

function funcRSgetStates(countryc){

    var rootURL = "/rest/api/location/getstates"
    var newarr = [];
    $("#state").empty();
    $("#city").empty();
    $("#state").append("<option value=''>Loading...</option>")
    for(i=0;i<isdCode.length;i++){
        if (isdCode[i].id == countryc) {
            $("#mobile1").val(isdCode[i].isd);
            $("#officephone").val(isdCode[i].isd);
        }
    }

    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"countryCode":"' +  countryc + '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            $("#state").empty();
            for (i=0; i<data.length; ++i) {
//                if (data[i].stateName == "") {
//                    $("#state").append("<option value='" + data[i].stateCode + "'>NA</option>")
//                } else {
                    $("#state").append("<option value='" + data[i].stateCode + "'>" + data[i].stateName + "</option>")
//                }
            }
            $('#state option').sort(NASort).appendTo('#state');
            $("#state")[0].selectedIndex = -1;

        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');
        }
    });

}

function funcRSgetCity(countryc, statec){

    var rootURL = "/rest/api/location/getcities"
    var newarr = [];
    $("#city").empty();
    $("#city").append("<option value=''>Loading...</option>")

    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"countryCode":"' +  countryc + '","stateCode":"' + statec + '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            $("#city").empty();
            for (i=0; i<data.length; ++i) {
                $("#city").append("<option value='" + data[i].cityName + "'>" + data[i].cityName + "</option>")
            }
            $('#city option').sort(NASort).appendTo('#city');
            $("#city")[0].selectedIndex = -1;
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(JSON.stringify(jqXHR));
            alert('Custom error: check logs');

        }
    });

}

function NASort(a, b) {
    if (a.innerHTML == 'NA') {
        return 1;
    }
    else if (b.innerHTML == 'NA') {
        return -1;
    }
    return (a.innerHTML > b.innerHTML) ? 1 : -1;
}

function funcGetUrlParam(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

function funcRSsubmitRegistration() {

    var rootURL = "/rest/registration/"
    var c = $('#company').val().replace('"', '');

    $.ajax({
        type: 'POST',
        crossDomain: true,
        contentType: 'application/json',
        url: rootURL,
        dataType: "json",
        data: '{"firstName":"' +  $('#firstName').val() +
                '","lastName":"' + $('#lastName').val() +
                '","company":{"companyName":"' + c +
                '"},"designation":"' + $('#designation').val() +
                '","email":"' + $('#email').val() +
                '","country":"' + $('#country').val() +
                '","countryCode":"' + $('#mobile1').val() +
                '","state":"' + $("#state option:selected").html() +
                '","city":"' + $('#city').val() +
                '","mobileNumber":"' + $('#mobile').val() +
                '","get2KnowAbtUs":"' + $('#howdidyouknow').val() +
                '","pinCode":"' + $('#zipcode').val() +
                '","officeNumber":"' + $('#officephone1').val() +
                '","requestKey":"' + $('#requestKey').val() +
              '"}',
        success: function(data, textStatus, jqXHR){
//            console.log(JSON.stringify(data));
            $('#beforeregister').hide();
            $('#lblbelowafterregister').html($('#firstName').val());
            $('#afterregister').show();
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(textStatus);
            $('#lblemail').addClass("has-error");
            alert('Email address already used.');
        }
    });



}




