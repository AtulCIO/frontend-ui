
	var CONFIG = (function ($, window) {
		
		var touch = Modernizr.touch,
			windowHeight = !touch ? $(window).height() : '650px' || '600px';
			
		return {

			/* ---------------------------------------------------- */
			/*	Main Settings										*/
			/* ---------------------------------------------------- */

			objOnePage : {
				easing: 'easeInOutExpo',			// Refer to the link below  http://easings.net/
				animatedElem: true,
				duration: 1200						// ms
			},

		}

	}(jQuery, window));
		
		
		