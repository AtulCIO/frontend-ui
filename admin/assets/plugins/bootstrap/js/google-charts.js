function drawcharts() {

            var data1 = google.visualization.arrayToDataTable(
                    [
          ['Year', 'Services'],
          ['2004',  10],
          ['2005',  15],
          ['2006',  11],
          ['2007',  15]
        ]);


            var options1 = {
                title: 'Activity Chart'

            };

            var data2 = google.visualization.arrayToDataTable(
		  			[
          ['Task', 'Subscriptions'],
          ['EB',     11],
          ['ED',      7],
          ['Collaborative Reports',  6],
          ['Content Library', 3],
        ]);


            var options2 = {
          title: 'Spend Pattern',
          is3D: true,
        };


            var chartA = new google.visualization.LineChart(document.getElementById('chart_activity'));
            chartA.draw(data1, options1);
		

            var chartB = new google.visualization.PieChart(document.getElementById('chart_spendpattern'));
            chartB.draw(data2, options2);
        }

        google.setOnLoadCallback(drawcharts);
        google.load("visualization", "1", {packages:["corechart"]});
		
		
	//Draw Network Map
	
	google.load("visualization", "1", {packages:["geochart"]});
    google.setOnLoadCallback(drawRegionsMap);


    function drawRegionsMap() {

        var data = google.visualization.arrayToDataTable([
          ['Country', 'Members'],
          ['Germany', 200],
          ['United States', 300],
          ['Brazil', 400],
          ['Canada', 500],
          ['France', 600],
          ['RU', 700]
        ]);

        var options = {
        colorAxis: {colors: ['#e7711c', '#4374e0']} // orange to blue
      };

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }

	
		
		
		
		
		