$(document).ready(function() {
    //toggle `popup` / `inline` mode
    $.fn.editable.defaults.mode = 'popup'; 
	    
    
    //make username editable
    $('#name').editable();
	$('#lastname').editable();
	$('#email').editable();
	$('#mobile').editable();
	$('#company').editable();
	$('#designation').editable();
	$('#location').editable();
	$('#comments').editable();
	$('#commentsmobile').editable();
	
	//make username editable
    $('#name1').editable();
	$('#lastname1').editable();
	$('#email1').editable();
	$('#mobile1').editable();
	$('#company1').editable();
	$('#designation1').editable();
	$('#location1').editable();
	$('#comments1').editable();
	$('#commentsmobile1').editable();
	
	//make username editable
    $('#name2').editable();
	$('#lastname2').editable();
	$('#email2').editable();
	$('#mobile2').editable();
	$('#company2').editable();
	$('#designation2').editable();
	$('#location2').editable();
	$('#comments2').editable();
	$('#commentsmobile2').editable();

    
    //make status editable
    $('#status').editable({
        type: 'select',
        title: 'Select status',
        placement: 'right',
        value: 2,
        source: [
            {value: 1, text: 'status 1'},
            {value: 2, text: 'status 2'},
            {value: 3, text: 'status 3'}
        ]
        /*
        //uncomment these lines to send data on server
        ,pk: 1
        ,url: '/post'
        */
    });
});